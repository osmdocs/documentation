# OpenStreetMap Mapping Documentation

This project contains the source for the OpenStreetMap Mapping Documentation project.

The aim of this project is to provide mappers with reliable and high-quality information on how to map features from the real world into the OpenStreetMap database, and how to use the OpenStreetMap data for their own downstream purposes.

# Principles

* The documentation must be reliable. When someone reads the documentation they should be confident that it is correct, and not have to check the edit history of the page to see who last edited it, who they are, and whether they trust that individual. This means that we operate on a 'proposed changes' model, and allows all proposed changes ti be discussed, modified, accepted or declined.
* The documentation must be inclusive. This means that anyone can propose changes, without any prior barriers like first being on a committee or team.
* The documenation must be multilingual. This means that translations are built-in to the system.
* The documenation must be consistent. This means that we manage proposed changes across multiple pages at the same time, not just editing individual pages in isolation. It also means that the documenation should be directly translated between languages, not reworked.
* The documenation should be forkable. This means that anyone should be able to take the work that we do, copy it, and recreate it elsewhere with no loss of information.

# Contributing

To contribute to this documentation, please make a pull request.

# Licence

The content of this documentation (text, images etc) is licensed under the [Creative Commons CC-BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/) licence, unless otherwise noted. This is the same licence as the OpenStreetMap Wiki, so content can be easily reused in either direction.

The code used to generate this documenation is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) licence.
